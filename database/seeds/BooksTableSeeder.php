<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
              [
                'title' => 'the end of faith',
                'author' => 'sam haris' ,
                'created_at' => date('Y-m-d G:i:s')
              ],
              [
                'title' => 'moral land scape',
                'author' => 'sam haris' ,
                'created_at' => date('Y-m-d G:i:s')
              ],
              [
                'title' => 'lying',
                'author' => 'sam haris' ,
                'created_at' => date('Y-m-d G:i:s')
              ],
              [
                'title' => 'waking up',
                'author' => 'sam haris' ,
                'created_at' => date('Y-m-d G:i:s')
              ],
              [
                'title' => 'free will',
                'author' => 'sam haris' ,
                'created_at' => date('Y-m-d G:i:s')
              ],
              ]);
    }
}
