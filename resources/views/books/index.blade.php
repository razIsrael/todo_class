<h1>This is your book list</h1>
<table>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Author</th>
        <th>Created At</th>
    </tr>
    @foreach($books as $book)
        <tr>
            <td> {{$book->id}}</td>
            <td> {{$book->title}}</td>
            <td> {{$book->author}}</td>
            <td> {{$book->created_at}}</td>
        </tr>    
    @endforeach
</table>